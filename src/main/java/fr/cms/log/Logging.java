package fr.cms.log;

import fr.cms.utils.Validate;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

public class Logging implements Log{

    private Logger logger;

    public Logging(Logger logger){
        this.logger = logger;
        System.setOut(IoBuilder.forLogger(logger).buildPrintStream());
    }

    @Override
    public void info(String message) {
        Validate.notNull(message);
        Validate.notEmpty(message);
        logger.info(message);
    }

    @Override
    public void error(String message) {
        Validate.notNull(message);
        Validate.notEmpty(message);
        logger.log(Level.ERROR, message);
    }

    @Override
    public void warning(String message) {
        Validate.notNull(message);
        Validate.notEmpty(message);
        logger.log(Level.WARN,message);
    }
}
