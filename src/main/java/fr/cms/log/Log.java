package fr.cms.log;

public interface Log {

    public void info(String message);

    public void error(String message);

    public void warning(String message);
}
