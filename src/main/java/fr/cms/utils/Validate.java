package fr.cms.utils;

public class Validate {

    private static final String NULLVALIDATOR = "The validated object is null";
    private static final String EMPTYVALIDATOR = "The validated object is empty";

    public static <T> T notNull(T object){
        return notNull(object,NULLVALIDATOR);
    }

    public static <T> T notNull(T object, String message, Object... format){
        if(object == null){
            throw new NullPointerException(String.format(message,format));
        }else{
            return object;
        }
    }

    public static <T> T notEmpty(T object){
        return notEmpty(object,EMPTYVALIDATOR);
    }

    public static <T> T notEmpty(T object, String message, Object... format){
        String string = (String) object;
        if (string.isEmpty()) {
            throw new IllegalArgumentException(String.format(message, format));
        } else {
            return object;
        }
    }
}
